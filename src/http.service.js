'use strict'

class Http {
  get(url) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest()
      xhr.open('get', url, true)
      xhr.responseType = 'json'
      xhr.onload = () => {
        var status = xhr.status
        if (status === 200) {
          return resolve(xhr.response)
        }
        return reject(status)
      }
      xhr.send()
    })
  }
}

export default new Http()