'use-strict'

import 'babel-polyfill'
import carousel from './carousel.controller'
import data from './data'

const pfCarousel = new carousel({
  element: document.getElementsByTagName('pf-carousel'),
  data: data.data
  // url: 'https://www.propertyfinder.ae/en/find-broker/ajax/search?page=1'
})
