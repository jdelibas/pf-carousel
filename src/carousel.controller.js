'use strict'

import './carousel.style.scss'
import http from './http.service'

export default class Carousel {
  constructor(options) {
      this.url = options.url
      this.data = options.data
      this.element = options.element
      this.prefix = options.prefix || 'pf'
      this.init()
  }

  async init() {
    try {
      const data = await this.loadData()
      this.render(data)
    } catch(e) {
      throw new Error(e)
    }
  }

  loadData() {
    if (this.url) {
      return http.get(this.url)
    }
    return this.data
  }

  render(data) {
    let element = this.element[0]
    let prefix = this.prefix
    let container = createContainer()
    let nav = createNav()
    let slides = []

    data.forEach((entry, i) => {
      container.appendChild(createSlide(entry, i))
    })

    element.appendChild(nav)
    element.appendChild(container)

    function createContainer() {
      let container = document.createElement('div')
      container.className += `${prefix}-carousel-container`
      return container
    }

    function createSlide(entry, i) {
      let slide = document.createElement('div')
      slide.className += `${prefix}-carousel-slide`
      slide.setAttribute('style', `order: ${i + 1}`)
      
      let slideTop = createSlideTop()
      slideTop.appendChild(createImage(entry.links.logo))
      slideTop.appendChild(createInfo(entry))
      slide.appendChild(slideTop)

      slide.appendChild(createDescription(entry))
      slides.push(slide)
      return slide
    }

    function createSlideTop() {
      let top = document.createElement('div')
      top.className += `${prefix}-carousel-slide-top`
      return top
    }             

    function createInfo(entry) {
      let info = document.createElement('div')
      info.className += `${prefix}-carousel-slide-info`
      info.innerHTML = 
      `<h3 class="name"> ${entry.name} </h3><p class="location"> from ${entry.location} </p>
        <p> ${entry.totalProperties} properties available now </p>
        <p><a href="tel:${entry.phone}">Call us on ${entry.phone}</a></p>
      `
      return info
    }             

    function createDescription(entry) {
      let description = document.createElement('div')
      description.className += `${prefix}-carousel-slide-description`
      description.innerHTML = 
      `<h3 class="title"> Description </h3>${entry.description}
      `
      return description
    }             

    function createImage(src) {
      let img = document.createElement('div')
      img.className += `${prefix}-carousel-slide-image`
      img.style['background-image'] = `url(${src})`
      return img
    }

    function createNav() {
      let buttons = document.createElement('div')
      // Previous Button
      let prev = document.createElement('button')
      prev.innerText = 'prev'
      prev.addEventListener('click', () => {
        navigate('prev')
      })
      buttons.appendChild(prev)
      // Next Button
      let next = document.createElement('button')
      next.innerText = 'next'
      next.addEventListener('click', () => {
        navigate('next')
      })
      buttons.appendChild(next)
      return buttons
    }

    function navigate(direction) {
      direction = direction || 'next'
      let mod = 1
      if (!direction || direction !== 'next' ) {
        mod = -1
      }
      slides.forEach((slide) => {
        let cache = parseInt(slide.style.order)
        let slideNum = parseInt(slide.style.order)
        if(slideNum >= slides.length && direction === 'next') {
          slideNum = 1
        } else if(slideNum === 1 && direction !== 'next') {
          slideNum = slides.length
        } else {
          slideNum += mod
        }
        slide.setAttribute('style', `order: ${slideNum}`)
      })
    }
  }
}
