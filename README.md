```
nvm use 6.9.1
npm install
npm run serve
```

When running against live data comment `data` option in `app.js` and uncomment `url`
then open ./dist/index.html in the browser directly to get past CORS on api

### Notes
- Using flexbox order was a mistake... for animation
- Attempted angular directive style structure
  - lots of dom caching
  - all controlled via references to the cache
  - made use of ES6 template strings
- No unit tests
- Not 100% responsive
- Too much time was spent on structuring project/boilerplate etc.
- Overcomplicated approach
- ES7 async/await is a bonus
- Vendor prefixes missing
